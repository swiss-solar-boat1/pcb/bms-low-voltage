#include <bq76952lib.h>

#define ALERT_PIN 10

bq76952 bms(ALERT_PIN);

void setup() {
  Serial.begin(9600);  // Initialize serial communication at 9600 baud rate
  bms.setDebug(false);
  bms.begin();

  bq76952_protection_t status = bms.getProtectionStatus();
  bq76952_temperature_t tStat = bms.getTemperatureStatus();

  Serial.println(status.bits.SC_DCHG);
  Serial.println(tStat.bits.OVERTEMP_FET);

  bms.setCellOvervoltageProtection(4000, 1000);
  bms.setCellUndervoltageProtection(3000, 1000);
  bms.setChargingOvercurrentProtection(50, 10);
  bms.setDischargingOvercurrentProtection(70, 15);
  bms.setDischargingShortcircuitProtection(SCD_40, 100);
  bms.setFET(DCH, ON);
}


void loop() {
  bool bms_charging = false;
  int bms_current = 0;
  unsigned int cell_voltages[16];  // Example for 16 cells, adjust the number according to your setup

  if (bms.isConnected()) {
    bms_charging = bms.isCharging();
    bms_current = bms.getCurrent();
    bms.getAllCellVoltages(cell_voltages);

    // Log charging status and current
    Serial.print("Charging: ");
    Serial.println(bms_charging ? "Yes" : "No");
    Serial.print("Current: ");
    Serial.print(bms_current);
    Serial.println(" mA");

    // Log individual cell voltages
    for (int i = 0; i < 16; i++) {  // Adjust loop to the number of cells
      Serial.print("Cell ");
      Serial.print(i + 1);
      Serial.print(" Voltage: ");
      Serial.print(cell_voltages[i]);
      Serial.println(" mV");
    }

    // Example for additional debugging information
    Serial.print("Internal Temperature: ");
    Serial.println(bms.getInternalTemp());

    Serial.print("Device ID: ");
    Serial.println(bms.getID());

    // Continue with CAN communication and eventual 7 segment display for the battery status.
  }

  delayMicroseconds(10000);
}